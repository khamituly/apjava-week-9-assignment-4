package domain.repositories;

import domain.book.Book;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class UserQuery {

    public List<Book.User> getUsers(Connection conn){
        List<Book.User> userList = new ArrayList<>();
        String sql = "SELECT * FROM users";
        try {
            Statement stm = conn.createStatement();
            ResultSet res = stm.executeQuery(sql);
            while (res.next()){
               userList.add(new Book.User(
                       res.getInt("id"),
                       res.getString("name"),
                       res.getString("username"),
                       res.getString("password"),
                       res.getInt("borrowedBooks_id"),
                       res.getString("role")
               )
               );
            }


        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return userList;
    }
}
