package domain.book;

public class Book {
    private int id;
    private int ISBN;
    private String name;
    private String author;
    private String pictureLink;
    private int amount;

    public Book(int id, int ISBN, String name, String author, String pictureLink,int amount) {
        this.id = id;
        this.amount= amount;
        this.ISBN = ISBN;
        this.name = name;
        this.author = author;
        this.pictureLink = pictureLink;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getISBN() {
        return ISBN;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getPictureLink() {
        return pictureLink;
    }

    public void setPictureLink(String pictureLink) {
        this.pictureLink = pictureLink;
    }

    public static class User {
        private int id;
        private String name;
        private String username;
        private String pass;
        private int borrowedBooks;
        private String role;

        public User(int id, String name, String username, String pass, int borrowedBooks,String role) {
            this.id = id;
            this.name = name;
            this.username = username;
            this.pass = pass;
            this.borrowedBooks = borrowedBooks;
        }

        public User(String name, String username, String pass, int borrowedBooks, String role) {
            this.name = name;
            this.username = username;
            this.pass = pass;
            this.borrowedBooks = borrowedBooks;
            this.role = role;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getPass() {
            return pass;
        }

        public void setPass(String pass) {
            this.pass = pass;
        }

        public int getBorrowedBooks() {
            return borrowedBooks;
        }

        public void setBorrowedBooks(int borrowedBooks) {
            this.borrowedBooks = borrowedBooks;
        }

        public String getRole() {
            return role;
        }

        public void setRole(String role) {
            this.role = role;
        }

        @Override
        public String toString() {
            return "name: "+name+", username:"+username;
        }
    }
}
