package controllers;

import DB.DB;
import domain.book.Book;
import domain.repositories.UserQuery;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import java.sql.Connection;
import java.util.List;


@Path("/hello")
public class HelloREST {

    @GET
    public Response getUsers() {
         DB db = new DB();
        Connection conn = db.getConnection();
        UserQuery userQuery = new UserQuery();
        List<Book.User> userList = userQuery.getUsers(conn);

         return Response.status(200).entity("hello ").build();
    }

}